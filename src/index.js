import _ from 'lodash';
import 'bulma/css/bulma.css';
import '@fortawesome/fontawesome-free/css/all.css'
import './styles/style.css';
import { tns } from "tiny-slider/src/tiny-slider";

document.addEventListener('DOMContentLoaded', function() {
    const loading = document.querySelector('.loader-content');
    setTimeout(() => {
        loading.classList.remove('isloading');
    }, 500);
    var slider = tns({
        container: '.slider-container',
        items: 3.3,
        mouseDrag: false,
        swipeAngle: false,
        speed: 400,
        lazyload: true,
        controls: false,
        prevButton: false,
        nextButton: false,
        nav: false,
        autoHeight: false,
        autoWidth: true,
        gutter: 15,
        loop: false,        
        responsive: {
            350: {
                items: 1,
                autoHeight: true,
                mouseDrag: true,
                autoWidth: true
            },
            900: {
                items: 3.3,
                mouseDrag: false,
                swipeAngle: false,
                speed: 400,
                lazyload: true,
                controls: false,
                prevButton: false,
                nextButton: false,
                nav: false,
                autoHeight: false,
                autoWidth: true,
                gutter: 15,
                loop: false
            }
        },
    });

    const images_array = document.querySelectorAll('.slider-container img');

    const imgs = document.images;

    let counter = 0;
    
    [].forEach.call( imgs, function( img ) {
        img.addEventListener( 'load', incrementCounter, false );
    } );
    
    function incrementCounter() {
        counter++;
        if ( counter >= images_array.length ) {
            let sum_width = 0;
            for ( const obj of images_array ) {
                sum_width = sum_width + obj.width;
                console.log(obj.width);
            }
            sum_width = sum_width + (images_array.length * 20);
            const sliderCont = document.querySelector('.slidercont');
            sliderCont.style.width = sum_width + "px";
            console.log( 'All images loaded!' );
        }
    }
    
    
    let isOnRight = false;
    let isOnLeft = false;
    
    const sliderCont = document.querySelector('.slidercont');
    
    function getMousePosition(e) {
        const width = document.body.clientWidth;
        const x = e.clientX;
        if ( x > (width / 2) ) {
            isOnRight = true;
            isOnLeft = false;
            sliderCont.classList.remove('leftmouse');
            sliderCont.classList.add('rightmouse');
        } else {
            isOnLeft = true;
            isOnRight = false;
            sliderCont.classList.remove('rightmouse');
            sliderCont.classList.add('leftmouse');
        }
    };
    
    function onClickSlider() {
        if ( isOnRight ) {
            slider.goTo('next');
        } 
        if ( isOnLeft ) {
            slider.goTo('prev');
        }
    }
    
    
    sliderCont.addEventListener('mousemove', (e) => { getMousePosition(e); });
    sliderCont.addEventListener('click', (e) => { onClickSlider(); });

    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    // Check if there are any navbar burgers
    if ($navbarBurgers.length > 0) {
        // Add a click event on each of them
        $navbarBurgers.forEach( el => {
            el.addEventListener('click', () => {
                // Get the target from the "data-target" attribute
                const target = el.dataset.target;
                const $target = document.getElementById(target);
                // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                el.classList.toggle('is-active');
                $target.classList.toggle('is-active');
            });
        });
    }
});


